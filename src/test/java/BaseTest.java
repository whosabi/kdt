import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;;
import java.time.Duration;

import static io.netty.util.internal.SystemPropertyUtil.contains;

public class BaseTest {
    WebDriver driver;
    @Before
    public void setup(){
        WebDriverManager.chromedriver().setup();

        ChromeOptions opt = new  ChromeOptions();
        opt.setHeadless(true);

        driver = new ChromeDriver(opt);
        driver.get("http://www.nesher-online.com");

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.manage().timeouts().scriptTimeout(Duration.ofSeconds(30));
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(60));

    }

    @Test
    public void assertTitle(){
        String currentTitle = driver.getTitle();
        String expectedTitle = "Nesher Online";
        Assert.assertTrue(currentTitle.contains(expectedTitle));
    }

    @Test
    public void assertPane(){
        Assert.assertTrue(true);
    }

    @Test
    public void assertPrivacy(){
        Assert.assertTrue(true);
    }
    @After
    public void tearDown(){
        driver.quit();
    }
}
